﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickControl : MonoBehaviour
{
    [SerializeField] private LayerMask targetLayer;

    public bool flight = false;
    
    public Transform top;
    public GameObject throwing;

    private float m_previousX;
    private float m_previousY;
    public float toplamDelta;
    public Animator anim;

    void Start()
    {
        anim.GetComponent<Animator>();
        anim.speed = 0;
        throwing.GetComponent<BoxCollider>().enabled = false;
    }

    private void Update()
    {
#if !MOBILE_INPUT
        Control_PC();
#else
            Control_Mobile();
#endif
    }
    private void Control_PC()
    {
        if (!flight)
        {
            if (Input.GetMouseButtonDown(0))
            {
                toplamDelta = 0.0f;
                m_previousX = Input.mousePosition.x;
            }
            if (Input.GetMouseButton(0) && m_previousX > Input.mousePosition.x)
            {
                float delta = (m_previousX - Input.mousePosition.x);
                m_previousX = Input.mousePosition.x;
                if (delta <= 5.0f)
                {
                    delta = 0;
                }
                anim.speed = delta / 100;
                toplamDelta += delta;
                if (toplamDelta >= 300)
                    toplamDelta = 300;              
            }
            if (Input.GetMouseButtonUp(0))
            {
                throwing.GetComponent<BoxCollider>().enabled = true;
                anim.speed = 1;
                anim.SetTrigger("StickRelease");
            }
        }
    }
}
