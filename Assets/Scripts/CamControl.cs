﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamControl : MonoBehaviour
{
    public GameObject stick;
    public GameObject Player;
    public GameObject child;
    public float speed;
    private void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        child = Player.transform.Find("camconst").gameObject;
    }
    void FixedUpdate()
    {
        if (stick.GetComponent<StickControl>().flight)
        {
            follow();
        }
    }
    private void follow()
    {
        //gameObject.transform.position = Vector3.Lerp(transform.position, child.transform.position, Time.deltaTime * speed);
        gameObject.transform.position = Vector3.Lerp(transform.position, Player.transform.position, Time.deltaTime * speed);
        gameObject.transform.LookAt(Player.gameObject.transform.position);
    }
}
