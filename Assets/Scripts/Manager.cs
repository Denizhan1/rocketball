﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    bool gameover = false;
    public void endgame()
    {
        if (!gameover)
        {
            gameover = true;
            Invoke("Restart", 2f);
        }
    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetSceneAt(0).name);
    }
}
