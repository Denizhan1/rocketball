﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Control : MonoBehaviour
{
    [SerializeField] private float controlSensivity = 0.2f;

    public GameObject ball;
    //public Transform bll;
    public GameObject sticktop;
    public GameObject stick;
    public Animator anim;
    public TMPro.TextMeshProUGUI Distance;
    private float toplam_delta;
    private float m_previousX;
    public bool ucuskontrol = false;
    void Start()
    {
        anim.GetComponent<Animator>().enabled = false;
    }
    void Update()
    {
        if (!stick.GetComponent<StickControl>().flight)
        {
            ball.transform.position = sticktop.transform.position;
            ball.transform.rotation = sticktop.transform.rotation;
        }
        else
        {
            Distance.text = "Distance: " + Convert.ToInt32(transform.position.z).ToString() + " m";
        }
        if (ucuskontrol)
        {
#if !MOBILE_INPUT
            ballControl_PC();
#else
                        ballControl_Mobile();
#endif
        }
    }
    void AnimationDisable()
    {
        GetComponent<Animator>().enabled = false;
    }
    void ballControl_PC()
    {
        if (Input.GetMouseButtonDown(0))
        {
            m_previousX = Input.mousePosition.x;
            anim.SetTrigger("wingsopen");
            Invoke("AnimationDisable", 0.5f);
        }
        if (Input.GetMouseButton(0) && !GetComponent<Animator>().enabled)
        {
            float delta = (Input.mousePosition.x - m_previousX) * controlSensivity;
            toplam_delta += delta;
            if (toplam_delta < 15 && toplam_delta > -15)
            {
                transform.Rotate(0, -delta * 3, 0);
            }
            GetComponent<Rigidbody>().velocity = new Vector3(toplam_delta, -5, 25);
            ball.transform.position += transform.up;
            m_previousX = Input.mousePosition.x;
        }
        if (Input.GetMouseButtonUp(0) && !GetComponent<Animator>().enabled)
        {
            toplam_delta = 0;
            GetComponent<Animator>().enabled = true;
            anim.SetTrigger("wingsclose");
            anim.SetTrigger("spin");
            ucuskontrol = false;
        }
    }
    void ballControl_Mobile()
    {
        if (Input.touchCount > 0)
        {
            Touch parmak = Input.GetTouch(0);
            if (parmak.phase == TouchPhase.Began)
            {
                m_previousX = Input.GetTouch(0).position.x;
                anim.SetTrigger("wingsopen");
                Invoke("AnimationDisable", 0.5f);
            }
            if (parmak.phase == TouchPhase.Moved && !GetComponent<Animator>().enabled)
            {
                float delta = (Input.GetTouch(0).position.x - m_previousX) * controlSensivity;
                toplam_delta += delta;
                if (toplam_delta < 15 && toplam_delta > -15)
                {
                    transform.Rotate(0, -delta * 3, 0);
                }
                GetComponent<Rigidbody>().velocity = new Vector3(toplam_delta, -5, 25);
                ball.transform.position += transform.up;
                m_previousX = Input.GetTouch(0).position.x;
            }
            if(parmak.phase == TouchPhase.Stationary && !GetComponent<Animator>().enabled)
            {
                GetComponent<Rigidbody>().velocity = new Vector3(toplam_delta, -5, 25);
                ball.transform.position += transform.up;
            }
            if (parmak.phase == TouchPhase.Ended && !GetComponent<Animator>().enabled)
            {
                toplam_delta = 0;
                GetComponent<Animator>().enabled = true;
                anim.SetTrigger("wingsclose");
                anim.SetTrigger("spin");
            }
        }
    }
    private void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Throw")
        {
            stick.GetComponent<StickControl>().flight = true;
            ucuskontrol = true;
            GetComponent<Rigidbody>().velocity = new Vector3(0, stick.GetComponent<StickControl>().toplamDelta / 20, stick.GetComponent<StickControl>().toplamDelta / 10);
            GetComponent<Animator>().enabled = true;
        }
    }
    private void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "cube")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 20, 20);
            if (!GetComponent<Animator>().enabled)
            {
                GetComponent<Animator>().enabled = true;
                anim.SetTrigger("wingsclose");
                anim.SetTrigger("spin");
            }
            ucuskontrol = true;
        }
        if (coll.gameObject.tag == "cylinder")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 40, 40);
            if (!GetComponent<Animator>().enabled)
            {
                GetComponent<Animator>().enabled = true;
                anim.SetTrigger("wingsclose");
                anim.SetTrigger("spin");
            }
            ucuskontrol = true;
        }
        if (coll.gameObject.tag == "terrain")
        {
            FindObjectOfType<Manager>().endgame();
        }
    }
}
